import React from 'react';
import { Platform, StatusBar, View, } from 'react-native';
import { AppLoading, Asset, Font, Icon } from 'expo';
import AppNavigator from './navigation/AppNavigator';
import * as Firebase from 'firebase';

const config = {
  apiKey: "AIzaSyCFV4CgcVTn1dI9RK98pdiqfpb5SUf4nsA",
  authDomain: "zzahlrechnerapp.firebaseapp.com",
  databaseURL: "https://zzahlrechnerapp.firebaseio.com",
  projectId: "zzahlrechnerapp",
  storageBucket: "zzahlrechnerapp.appspot.com",
  messagingSenderId: "188239131672"
};

export default class App extends React.Component {
    state = {
      isLoadingComplete: false,
    };

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <View style={{flex:1}}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          <AppNavigator />
        </View>
      );
    }
  }

  _loadResourcesAsync = async () => {

    Firebase.initializeApp(config);

    return Promise.all([
      Asset.loadAsync([
        require('./assets/images/robot-dev.png'),
        require('./assets/images/robot-prod.png'),
        require('./assets/sounds/diamond.mp3'),
        require('./assets/images/diamond.png'),
        require('./assets/images/filler.png'),
        require('./assets/images/fraisl.jpg'),
        require('./assets/images/haemmerle.jpg'),
        require('./assets/images/mohr.jpg'),
        require('./assets/images/somschor.jpg'),
      ]),
      Font.loadAsync({
        ...Icon.Ionicons.font,
      }),
    ]);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}
