import React from 'react';
import {
  View,
  TextInput,
  Alert,
  Button,
  Picker,
  Text
} from 'react-native';

import * as Firebase from 'firebase';

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'Rechner',
    headerStyle: { marginTop: -30 },
  };
  state = {
    Age: '',
    Height: '',
    Weight: '',
    ShoeLength: '',
    SkiType: '',
  }

  onSubmitEditingAge = () => {
    this.refs.Height.focus();
  }

  onSubmitEditingHeight = () => {
    this.refs.Weight.focus();
  }

  onSubmitEditingWeight = () => {
    this.refs.ShoeLength.focus();
  }

  info = () => {
    alert(Firebase.auth().currentUser.email);
  }

  logout = () => {
    Firebase.auth().signOut();
    this.props.navigation.navigate('Login');
  }

  calculate = () => {
    if(this.state.Age < 120 && this.state.Age > 0) {
      if(this.state.Height < 250 && this.state.Height > 60){
        if(this.state.Weight < 120 && this.state.Weight > 20)  {
          if(this.state.ShoeLength < 400 && this.state.ShoeLength > 200) {
            if(this.state.SkiType == 1 || this.state.SkiType == 2 || this.state.SkiType == 3) {
              Alert.alert(
                'Ergebnis',
                '*ergebnisvariable*',
                [
                  {text: 'OK'},
                ],
                {cancelable: false},
              )
            }
            else {
                Alert.alert(
                  'Alert',
                  'Please specify a skitype',
                  [
                    {text: 'OK'},
                  ],
                  {cancelable: false},
                )
            }
          } 
          else {
            Alert.alert(
              'Alert',
              'Please correct the specified shoelenght',
              [
                {text: 'OK'},
              ],
              {cancelable: false},
            )
          }
        } 
        else { 
          Alert.alert(
            'Alert',
            'Please correct the specified weight',
            [
              {text: 'OK'},
            ],
            {cancelable: false},
          )
        }
      } 
      else {
        Alert.alert(
          'Alert',
          'Please correct the specified height',
          [
            {text: 'OK'},
          ],
          {cancelable: false},
        )
      }
    } 
    else { 
      Alert.alert(
        'Alert',
        'Please correct the specified age',
        [
          {text: 'OK'},
        ],
        {cancelable: false},
      )
    }
  }
  
  render() {
    return (
      <View style={{ margin: 10}}>
        <Text>Age:</Text>
        <Picker
          selectedValue={this.state.Age}
          onValueChange={(itemValue) => this.setState({Age: itemValue})}>
          <Picker.Item label = '<9' value= '1' />
          <Picker.Item label = '10 - 49' value= '2' />
          <Picker.Item label = '>50' value= '3' />
        </Picker>

        <Text>Height:</Text>
        <Picker
          selectedValue={this.state.Age}
          onValueChange={(itemValue) => this.setState({Height: itemValue})}>
          <Picker.Item label = '<148' value= '1' />
          <Picker.Item label = '149 - 157' value= '2' />
          <Picker.Item label = '50+' value= '3' />
        </Picker>
        
        <Text>Skitype:</Text>
        <Picker
          selectedValue={this.state.SkiType}
          onValueChange={(itemValue) => this.setState({SkiType: itemValue})}>
          <Picker.Item label = 'Beginner' value= '1' />
          <Picker.Item label = 'Intermediate' value= '2' />
          <Picker.Item label = 'Expert' value= '3' />
        </Picker>
        <Button
          onPress = {this.calculate}
          title = 'calculate' />
          <Button
          onPress = {this.info}
          title = 'info' />
          <Button
          onPress = {this.logout}
          title = 'logout' />
      </View>
    );
  }
}
