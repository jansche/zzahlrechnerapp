import React from 'react';
import {
  Image,
  View,
  Text
} from 'react-native';

export default class HomeScreen extends React.Component {

  static navigationOptions = {
    header: null,
  }; 
  
  render() {
      return (
        <View style={{backgroundColor: 'black', justifyContent: 'center', padding: 6}}>
          <Text>
            Developer team: Christoph Faißt
                            Jan Hagen
          </Text>
        </View>
    );
  }

}
