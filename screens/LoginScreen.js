import React, { Component } from 'react';

import {
    ScrollView,
    TextInput,
    View,
    Button,
    KeyboardAvoidingView,
    Alert
} from 'react-native';
import * as Firebase from 'firebase';

export default class Login extends Component {
  static navigationOptions = {
    title: 'Login',
  }
  state = {
    EMailAddress: '',
    Password: '',
  }

  onLoginPress = () => {
    Firebase.auth().signInWithEmailAndPassword(this.state.EMailAddress, this.state.Password)
    .then(user => {if(Firebase.auth().currentUser.emailVerified) {
                    this.props.navigation.navigate('Calculator');
                    }
                  else {
                    Alert.alert(
                      'Warnung',
                      'Sie habe ihre E-Mail Adresse noch nicht bestätigt.',
                      [
                        {text: 'OK'},
                        {text: 'Bestätigung erneut senden',
                         onPress: this.verify },
                      ],
                      {cancelable: false},
                    )
                  }
                  } )
    .catch(function(error) {
      Alert.alert(
        'Warnung',
        'Uns liegt diese E-Mail - Passwort Kombination nicht vor.',
        [
          {text: 'OK'},
        ],
        {cancelable: false},
      )
    });
  }

  onRegisterPress = () => {
    this.props.navigation.navigate('Register');
  }

  verify = () => {
    try {
      Firebase.auth().currentUser.sendEmailVerification();
    }
    catch {
      Alert.alert(
        'Warnung',
        'Das senden der E-Mail ist uns nicht möglich. Versuchen Sie es später nochmal.',
        [
          {text: 'OK'},
        ],
        {cancelable: false},
      )
    }
  }


  onSubmitEditing = () => {
    this.refs.Password.focus();
  }

  render() {
    return (
      <ScrollView style={{ padding: 20 }}>
        <TextInput placeholder='E-Mail Address' style={{ margin: 20 }} ref={'EMailAddress'} returnKeyType="next" onSubmitEditing={this.onSubmitEditing} onChangeText={(EMailAddress) => this.setState({EMailAddress})} keyboardType={'email-address'}/>
        <TextInput placeholder='Password' style={{ margin: 20 }} ref={'Password'} secureTextEntry={true} onSubmitEditing={this.onLoginPress} onChangeText={(Password) => this.setState({Password})}/>
        <View style={{ margin: 10 }} />
        <KeyboardAvoidingView behavior="padding">
          <View>
            <Button 
              onPress={this.onLoginPress}
              title="Log me in"
              ref={'Submit'}
              color={'#0072ff'}
            />
           </View>
           <View style={{ marginTop: 340 }}>
            <Button 
                onPress={this.onRegisterPress}
                title='Sign me up'
                ref={'Register'}
                color={'#0072ff'}
              />
           </View>
           
        </KeyboardAvoidingView>
      </ScrollView> 
      );
  }
}
