import React, { Component } from 'react';

import {
    ScrollView,
    TextInput,
    View,
    Button,
    KeyboardAvoidingView,
    Alert,
    ActivityIndicator,
} from 'react-native';

import * as Firebase from 'firebase';

export default class Register extends Component {
  static navigationOptions = {
    title: 'Register',
  }
  state = {
    EMailAddress: '',
    Password: '', 
    animating: false
  }

   


  onRegisterPress = () => {
      this.state.animating = true;
      Firebase.auth().createUserWithEmailAndPassword(this.state.EMailAddress, this.state.Password)
      .then(user => { Firebase.auth().signInWithEmailAndPassword(this.state.EMailAddress, this.state.Password);
                      if(user) {
                        Firebase.auth().currentUser.sendEmailVerification();
                        Alert.alert(
                          'Warnung',
                          'Bitte bestätigen Sie Ihre E-Mail Adresse vor dem nächsten Login', 
                          [
                            {text: 'OK', onPress: () => {this.props.navigation.navigate('Calculator')}}
                          ],
                          {cancelable: true},
                        )
                        }})
      .catch(error => {
         switch (error.code) {
            case 'auth/email-already-in-use':
              Alert.alert(
                'Alert',
                'Email address is already in use.', 
                [
                  {text: 'Cancel'},
                  {text: 'Login', onPress: () => this.props.navigation.navigate('Login') }
                ],
                {cancelable: true},
              )
              break;
            case 'auth/invalid-email':
              alert('Email address is invalid.');
              break;
            case 'auth/operation-not-allowed':
              alert('Error during sign up.');
              break;
            case 'auth/weak-password':
              alert('Password is not strong enough. Add additional characters including special characters and numbers.');
              break;
            //default: Firebase.auth().signInWithEmailAndPassword(this.state.EMailAddress, this.state.Password);
                      //this.props.navigation.navigate('Calculator')
                    }
          }
      );
  }
  
  onSubmitEditingEMail = () => {
    this.refs.Password.focus();
  }

  render() {
    return (
      <ScrollView style={{ padding: 20 }}>
        <TextInput placeholder='E-Mail Adresse' style={{ margin: 20 }} ref={'EMailAddress'} onSubmitEditing={this.onSubmitEditingEMail} onChangeText={(EMailAddress) => this.setState({EMailAddress})} keyboardType={'email-address'}/>
        <TextInput placeholder='Passwort' secureTextEntry={true} style={{ margin: 20 }} ref={'Password'} onSubmitEditing={this.onRegisterPress} onChangeText={(Password) => this.setState({Password})}/>
        <View style={{ margin: 10 }} />
        <KeyboardAvoidingView behavior="padding">
          <Button 
            onPress={this.onRegisterPress}
            title='Register'
            ref={'Register'} />
        </KeyboardAvoidingView>
        <ActivityIndicator size='large' animating={this.state.animating} />
      </ScrollView> 
      );
  }
}
