import React from 'react';
import { createSwitchNavigator, createStackNavigator, createAppContainer, createBottomTabNavigator } from 'react-navigation';
import { Platform } from 'react-native';
import TabBarIcon from '../components/TabBarIcon';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import CalculatorScreen from '../screens/CalculatorScreen';
import InfoScreen from '../screens/InfoScreen';

const LoginStack = createStackNavigator({ Login: LoginScreen });
const RegisterStack = createStackNavigator({ Register: RegisterScreen});
const CalculatorStack = createStackNavigator({ Calculator: CalculatorScreen});
const InfoStack = createStackNavigator({ Info: InfoScreen});

CalculatorStack.navigationOptions = {
  tabBarLabel: 'Rechner',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  ),
}; 

InfoStack.navigationOptions = {
  tabBarLabel: 'Info',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  ),
};

const BottomTabNavigator = createBottomTabNavigator({
  CalculatorStack,
  InfoStack,
});

const TabNavigatorStack = createStackNavigator({
  BottomTabNavigator: BottomTabNavigator
});

const SwitchNavigator = createSwitchNavigator(
  {
    Login: LoginStack,
    Register: RegisterStack,
    Tabs: TabNavigatorStack,
  },
  { 
    initialRouteName: 'Login',
  }
);

const AppContainer = createAppContainer(SwitchNavigator, BottomTabNavigator);

export default AppContainer;
